ruby "2.3.1"
source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.0.0'
# Use postgresql as the database for Active Record
gem 'pg', '~> 0.18'
# Use Puma as the app server
gem 'puma', '~> 3.0'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Gems adicionadas para validar o CNPJ e o e-mail
gem 'email_validator', '~> 1.6'
gem 'validates_cnpj', '~> 2.0', '>= 2.0.1'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platform: :mri
  # Adicionei o rspec pois pelo que entendi é a gem de testes utilizada pelo PetBooking
  gem 'rspec-rails', '~> 3.5'
  # Para seguir a stack comum de quem usa o Rspec, adicionei o factory_girl_rails
  # ao invés de usar fixtures
  gem 'factory_girl_rails', '~> 4.7'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console'
  gem 'listen', '~> 3.0.5'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  # Adicionei a shoulda-matchers pois facilita a escrita de alguns testes repetitivos.
  # Nesse teste não iremos repetir tanto código mas conforme a aplicação cresce os testes de
  # validação acabam se repetindo muito, por isso gosto de usar a shoulda-matchers
  gem 'shoulda-matchers', '~> 3.1', '>= 3.1.1'
  # Essa gem tem alguns matchers que facilitam o teste de um controller
  gem 'rails-controller-testing', '~> 1.0', '>= 1.0.1'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
