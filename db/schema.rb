# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160927214600) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "citext"

  create_table "professionals", force: :cascade do |t|
    t.string   "name",                  null: false
    t.citext   "email",                 null: false
    t.string   "phone",      limit: 11
    t.string   "address"
    t.string   "cnpj",       limit: 14
    t.string   "crmv",       limit: 5
    t.string   "type",                  null: false
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.index ["cnpj"], name: "index_professionals_on_cnpj", unique: true, using: :btree
    t.index ["crmv"], name: "index_professionals_on_crmv", unique: true, using: :btree
    t.index ["email"], name: "index_professionals_on_email", unique: true, using: :btree
  end

end
