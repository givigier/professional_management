class CreateProfessional < ActiveRecord::Migration[5.0]
  def change
    create_table :professionals do |t|
      t.string :name, null: false
      t.citext :email, null: false
      t.string :phone, limit: 11
      t.string :address
      t.string :cnpj, limit: 14
      t.string :crmv, limit: 5
      t.string :type, null: false

      t.timestamps
    end
    add_index :professionals, :email, unique: true
    add_index :professionals, :cnpj, unique: true
    add_index :professionals, :crmv, unique: true
  end
end
