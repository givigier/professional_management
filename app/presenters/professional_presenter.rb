class ProfessionalPresenter < SimpleDelegator
  attr_reader :professional

  def initialize(professional)
    @professional = professional
    __setobj__(professional)
  end

  def document
    case @professional.type
    when "IndependentProfessional" then @professional.crmv
    when "LegalPerson" then @professional.cnpj
    else nil end
  end
end
