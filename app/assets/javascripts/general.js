document.addEventListener("turbolinks:load", function() {
  hideProfessionRoles();
  openProfessionalRoles();

  $(".registration input[name='professional[type]']").on("click", function() {
    hideProfessionRoles();
    $("." + mapRolesValues(this)).show();
  });

  function hideProfessionRoles() {
    $(".registration .independent_professional").hide();
    $(".registration .legal_person").hide();
  }

  function mapRolesValues(self) {
    roles = {
      "IndependentProfessional": "independent_professional",
      "LegalPerson": "legal_person "
    }
    return roles[$(self).val()]
  }

  function openProfessionalRoles() {
    var self = $(".registration input[name='professional[type]']:checked");
    $("." + mapRolesValues(self)).show();
  }
});
