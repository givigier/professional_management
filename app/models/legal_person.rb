class LegalPerson < Professional
  validates :cnpj, uniqueness: true, cnpj: true, presence: true

  before_save :format_cnpj

  private
  def format_cnpj
    self.cnpj = cnpj.to_s.gsub(/\D/, "")
  end
end
