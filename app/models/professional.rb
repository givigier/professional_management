class Professional < ApplicationRecord
  validates :name, presence: true
  validates :email, presence: true, uniqueness: { case_sensitive: true }, email: true
  validates :phone,
    numericality: { only_integer: true },
    length: { in: 10..11 },
    allow_blank: true
end
