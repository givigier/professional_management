class IndependentProfessional < Professional
  validates :crmv,
    presence: true,
    numericality: { only_integer: true },
    uniqueness: true,
    length: { is: 5 }
end
