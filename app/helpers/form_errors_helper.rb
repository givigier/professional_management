module FormErrorsHelper
  def format_errors(object)
    messages = []
    object.errors.messages.each do |key, errors|
      errors.each{ |e| messages << "#{ key.to_s.humanize } #{ e }" }
    end
    messages
  end
end
