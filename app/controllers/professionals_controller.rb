class ProfessionalsController < ApplicationController
  def show
    @professional = ProfessionalPresenter.new(Professional.find(params[:id]))
  end

  def new
    @professional = Professional.new
  end

  def create
    @professional = Professional.new(professional_params)
    if @professional.save
      redirect_to professional_path(@professional)
    else
      render :new
    end
  end

  private
  def professional_params
    params.require(:professional)
      .permit(:name, :email, :phone, :address, :crmv, :cnpj, :type)
      .reject{|_, v| v.blank? }
  end
end
