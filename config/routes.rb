Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :professionals, only: [:show, :new, :create]

  root "professionals#new"
end
