require 'rails_helper'

RSpec.describe ProfessionalsController do
  # Por que não testou as actions new e show?
  # Essas actions são muito simples, nos testes de controller prefiro fazer
  # testes de integração ao invés de ficar verificando se a variável @professional
  # tem o valor correto e se a view correta está sendo renderizada.
  context "POST#create" do
    let(:professional) { attributes_for(:legal_person, name: "") }

    describe "with invalid attributes" do
      it "does not save the new professional in the database" do
        expect{
          post :create, params: { professional: professional }
        }.to_not change(LegalPerson,:count)
      end

      it "re-renders the :new template" do
        post :create, params: { professional: professional }
        expect(response).to render_template "professionals/new"
      end
    end

    describe "with valid attributes" do
      let(:professional) { attributes_for(:legal_person) }

      it "saves the new professional in the database" do
        expect{
          post :create, params: { professional: professional }
        }.to change(LegalPerson,:count).by(1)
      end

      it "redirects to the show page" do
        post :create, params: { professional: professional }
        expect(response).to redirect_to professional_path(LegalPerson.last)
      end
    end
  end
end
