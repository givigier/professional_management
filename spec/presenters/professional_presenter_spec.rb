require 'rails_helper'

RSpec.describe ProfessionalPresenter do
  context "#document" do
    it "of independent_professional" do
      professional = build_stubbed(:independent_professional)
      expect(ProfessionalPresenter.new(professional).document).to eq professional.crmv
    end

    it "of legal_person" do
      professional = build_stubbed(:legal_person)
      expect(ProfessionalPresenter.new(professional).document).to eq professional.cnpj
    end
  end
end
