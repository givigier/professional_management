require 'rails_helper'

RSpec.describe Professional do
  context "when validating" do
    let(:professional){ create(:independent_professional) }

    it { expect(professional).to validate_presence_of(:name) }
    it { expect(professional).to validate_presence_of(:email) }
    it { expect(professional).to validate_uniqueness_of(:email).case_insensitive }
    it { expect(professional).to validate_numericality_of(:phone) }
    it { expect(professional).to validate_length_of(:phone).is_at_least(10).is_at_most(11) }
  end
end
