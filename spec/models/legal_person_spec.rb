require 'rails_helper'

RSpec.describe LegalPerson do
  context "when validating" do
    let(:professional){ create(:legal_person) }

    it { expect(professional).to validate_uniqueness_of(:cnpj).case_insensitive }

    describe "ensure that cnpj" do
      it "is valid with correct format" do
        professional = build_stubbed(:legal_person, cnpj: "10.122.188/0001-02")
        expect(professional).to be_valid
      end

      it "is invalid with wrong format" do
        professional = build_stubbed(:legal_person, cnpj: "10.122.188/0001-01")
        expect(professional).to be_invalid
      end
    end

    describe "#format_cnpj" do
      it "when formatting maintain only numbers" do
        professional = build(:legal_person, cnpj: "10.122.188/0001-02")
        professional.save!

        expect(professional.cnpj).to eq "10122188000102"
      end
    end
  end
end
