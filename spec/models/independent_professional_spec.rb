require 'rails_helper'

RSpec.describe IndependentProfessional do
  context "when validating" do
    let(:professional){ create(:independent_professional) }

    it { expect(professional).to validate_numericality_of(:crmv) }
    it { expect(professional).to validate_length_of(:crmv).is_equal_to(5) }
    it { expect(professional).to validate_uniqueness_of(:crmv).case_insensitive }
  end
end
