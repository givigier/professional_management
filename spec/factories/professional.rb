FactoryGirl.define do
  factory :professional do
    name "Joao"
    sequence(:email) { |n| "joao#{ n }@gmail.com" }
    phone "31222222222"
    address "Av. do Contorno 30, Funcionários, Belo Horizonte - MG"

    factory :independent_professional, class: IndependentProfessional do
      crmv "12345"
      type "IndependentProfessional"
    end

    factory :legal_person, class: LegalPerson do
      cnpj "10122188000102"
      type "LegalPerson"
    end
  end
end
